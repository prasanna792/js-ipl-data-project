const express = require('express');
const path = require('path');
const app = express();
const rootRoute = require(path.resolve(__dirname, 'routes/root-route.cjs'));
const problem1 = require(path.resolve(__dirname,'routes/problem1-route.cjs'));
const problem2 = require(path.resolve(__dirname,'routes/problem2-route.cjs'));
const problem3 = require(path.resolve(__dirname,'routes/problem3-route.cjs'));
const problem4 = require(path.resolve(__dirname,'routes/problem4-route.cjs'));
const problem5 = require(path.resolve(__dirname,'routes/problem5-route.cjs'));
const problem6 = require(path.resolve(__dirname,'routes/problem6-route.cjs'));
const problem7 = require(path.resolve(__dirname,'routes/problem7-route.cjs'));
const problem8 = require(path.resolve(__dirname,'routes/problem8-route.cjs'));
const problem9 = require(path.resolve(__dirname,'routes/problem9-route.cjs'));

const PORT = process.env.PORT || 8000

app.use(express.json());

app.use(rootRoute);
app.use(problem1);
app.use(problem2);
app.use(problem3);
app.use(problem4);
app.use(problem5);
app.use(problem6);
app.use(problem7);
app.use(problem8);
app.use(problem9);

app.listen(PORT,() => {
    console.log(`server is listening on port ${PORT}`);

});


