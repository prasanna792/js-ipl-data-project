
function bestEconomyInSuperOvers(deliveriesData) {

    let superOvers = deliveriesData.reduce((accumulator, delivery) => {

        if (delivery.is_super_over != 0) {
            if (accumulator[delivery.bowler]) {
                accumulator[delivery.bowler].total_balls += 1
                accumulator[delivery.bowler].total_runs += Number(delivery.total_runs)
                accumulator[delivery.bowler]['economy'] = Number((accumulator[delivery.bowler].total_runs / (accumulator[delivery.bowler].total_balls / 6)).toFixed(2))

            } else {
                accumulator[delivery.bowler] = {
                    'total_balls': 1,
                    'total_runs': Number(delivery.total_runs)
                }
            }
        }
        return accumulator
    }, {})

    // console.log(superOvers);
    let bestEconomyPlayers = Object.entries(superOvers).sort(([, bowler1], [, bowler2],) => {
        return bowler1.economy - bowler2.economy
    });

    return bestEconomyPlayers[0];

}

module.exports = {
    bestEconomyInSuperOvers
}