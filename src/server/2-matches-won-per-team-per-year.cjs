// Number of matches won per team per year in IPL.



function matchesWonPerTeamPerYear(matches) {

    let matchesWon = matches.reduce((accumalator, match) => {
        if (accumalator[match.season]) {
            if (accumalator[match.season][match.winner]) {
                accumalator[match.season][match.winner]++;
            } else {
                accumalator[match.season][match.winner] = 1;
            }
        } else {
            accumalator[match.season] = {};
            accumalator[match.season][match.winner] = 1;
        }
        return accumalator;

    }, {})

    return matchesWon;

}

module.exports = {
    matchesWonPerTeamPerYear
};




