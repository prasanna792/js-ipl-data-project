// Extra runs conceded per team in the year 2016


function extraRunsConcededPerTeam(matches, deliveries) {
    const matchesId2016 = matches.filter((match) => {
        return match.season.includes('2016');
    }).map((item) => {
        return item.id;
    })

    let result = deliveries.reduce((accumulator, item) => {
        if (matchesId2016.includes(item.match_id)) {
            if (accumulator[item.bowling_team]) {
                accumulator[item.bowling_team] += Number(item.extra_runs);
            } else {
                accumulator[item.bowling_team] = Number(item.extra_runs);
            }
        }
        return accumulator;
    }, {})

    return result;

}


module.exports = {
    extraRunsConcededPerTeam
}

