// Find the strike rate of a batsman for each season



const strikeRate = (matches, deliveries) => {
    const matchesSeason = matches.reduce((accumulater, match) => {
        accumulater[match.id] = match.season;
        return accumulater;
    }, {});
    // console.log(matchesSeason);
    const playersStrikeRateDetails = deliveries.reduce((accumulater, delivery) => {
        // console.log(delivery)
        let season = matchesSeason[delivery.match_id];
        if (accumulater[delivery.batsman]) {
            if (accumulater[delivery.batsman][season]) {
                accumulater[delivery.batsman][season].totalRuns += parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced += 1;
                accumulater[delivery.batsman][season].strikeRate = (accumulater[delivery.batsman][season].totalRuns / accumulater[delivery.batsman][season].totalBallsFaced) * 100;
            }
            else {
                accumulater[delivery.batsman][season] = {};
                accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
                accumulater[delivery.batsman][season].totalBallsFaced = 1;
                accumulater[delivery.batsman][season].strikeRate = (accumulater[delivery.batsman][season].totalRuns / accumulater[delivery.batsman][season].totalBallsFaced) * 100;
            }
        }
        else {
            accumulater[delivery.batsman] = {};
            accumulater[delivery.batsman][season] = {};
            accumulater[delivery.batsman][season].totalRuns = parseInt(delivery.total_runs);
            accumulater[delivery.batsman][season].totalBallsFaced = 1;
            accumulater[delivery.batsman][season].strikeRate = (accumulater[delivery.batsman][season].totalRuns / accumulater[delivery.batsman][season].totalBallsFaced) * 100;
        }
        return accumulater;
    }, {});
   
    return playersStrikeRateDetails;


}

module.exports = {
    strikeRate
}

