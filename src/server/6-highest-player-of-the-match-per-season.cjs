// Find a player who has won the highest number of Player of the Match awards for each season


function playerOfTheMatch(matchesData = []) {

    let seasonPlayerOfMatch = matchesData.reduce((accumulater, current) => {
        if (accumulater[current.season]) {
            if (accumulater[current.season][current.player_of_match]) {
                accumulater[current.season][current.player_of_match]++;
            }
            else {
                accumulater[current.season][current.player_of_match] = 1;
            }
        }
        else {
            accumulater[current.season] = {};
            accumulater[current.season][current.player_of_match] = 1;
        }
        return accumulater;

    }, {})


    let playerOfTheMatchAward = Object.fromEntries(Object.entries(seasonPlayerOfMatch).map((data) => {
        let sortedData = Object.entries(data[1]).sort((player1, player2) => {
            return player2[1] - player1[1]
        })[0]
        let answer = [data[0], sortedData];
        return answer;
    }))


    return playerOfTheMatchAward;

}


module.exports = {
    playerOfTheMatch
}
