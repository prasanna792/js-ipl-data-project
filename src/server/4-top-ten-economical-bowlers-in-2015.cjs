// Top 10 economical bowlers in the year 2015

function topTenEconomicalBowlersIn2015(matchesData, deliveriesData) {

  let yearIds = matchesData.reduce((acc, matches) => {
    if (matches.season.includes("2015")) {
      acc.push(matches.id)
    }
    return acc;
  }, [])

  const economyData = deliveriesData.reduce((accumulator, deliveries) => {

    if (yearIds.includes(deliveries.match_id)) {
      if (accumulator[deliveries.bowler]) {
        accumulator[deliveries.bowler]['balls'] += 1;
        accumulator[deliveries.bowler]['runs'] += Number(deliveries.total_runs)
        accumulator[deliveries.bowler]['economy'] = Number((accumulator[deliveries.bowler]['runs'] / (accumulator[deliveries.bowler]['balls'] / 6)).toFixed(2))

      } else {
        accumulator[deliveries.bowler] = {}
        accumulator[deliveries.bowler]['balls'] = 1
        accumulator[deliveries.bowler]['runs'] = Number(deliveries.total_runs)
      }
    }
    return accumulator;
  }, {})

  const topTenEconomicalBowlers = Object.fromEntries(Object.entries(economyData).sort(([, player1], [, player2]) => {
    return player1.economy - player2.economy
  }).slice(0, 10))

  return topTenEconomicalBowlers;

}

module.exports = {
  topTenEconomicalBowlersIn2015
}