// // Number of matches played per year for all the years in IPL.


function matchesPerYear(matches) {
    let matchesPerYearData = matches.reduce((accumulator, match) => {
        if (accumulator[match.season]) {
            accumulator[match.season]++;
        } else {
            accumulator[match.season] = 1;
        }
        return accumulator;
    }, {});
    return matchesPerYearData;
}

module.exports = {
    matchesPerYear
};
