// Find the highest number of times one player has been dismissed by another player


function dismissedByAnotherPlayer(deliveriesData) {

  const dismissalData = deliveriesData.reduce((result, player) => {

    if (player.player_dismissed) {

      if ((result[player.player_dismissed] && result[player.player_dismissed][player.bowler])) {
        result[player.player_dismissed][player.bowler] += 1;

      } else if (result[player.player_dismissed] == undefined) {
        result[player.player_dismissed] = {}
        result[player.player_dismissed][player.bowler] = 1
      } else {
        result[player.player_dismissed][player.bowler] = 1
      }
    }
    return result
  }, {})

  let sortedAwards = Object.entries(dismissalData).reduce((result, data) => {

    let playerData = Object.entries(data[1]).sort(([, player1], [, player2]) => {
      return player2 - player1
    })[0]

    result[data[0]] = {
      "dismissedPlayer": playerData[0],
      "numberOfdismissals": playerData[1]
    }

    return result;

  }, {})

  const answer = Object.entries(sortedAwards).sort(([key1, item1], [key2, item2]) => {
    return item2.numberOfdismissals - item1.numberOfdismissals
  })[0];

  return answer;
}


module.exports = {
  dismissedByAnotherPlayer
}


