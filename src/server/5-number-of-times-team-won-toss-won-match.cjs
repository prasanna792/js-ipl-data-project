// Find the number of times each team won the toss and also won the match

const numberOfTimesTeamWonTossAndMatch = (matches) => {
    const teamsWon = matches.reduce((accumulater, match) => {
        if (accumulater[match.toss_winner]) {
            if (match.toss_winner === match.winner) {
                accumulater[match.toss_winner] += 1;
            }
        }
        else {
            if (match.toss_winner === match.winner) {
                accumulater[match.toss_winner] = 1;
            }
        }
        return accumulater;
    }, {});

    return teamsWon;
    //console.log(teamsWon);
}

module.exports = {
    numberOfTimesTeamWonTossAndMatch
}
