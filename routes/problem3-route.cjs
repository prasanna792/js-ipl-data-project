const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
let deliveriesFilePath = path.resolve(__dirname, '../src/data/deliveries.csv');
const { extraRunsConcededPerTeam } = require('../src/server/3-extra-runs-conceded-per-team-in-2016.cjs')


router.get('/problem3', (req, res) => {
    csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
        csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
            let resultExtraRuns = extraRunsConcededPerTeam(matchesFile, deliveriesFile)
              res.send(resultExtraRuns)
            // fs.writeFileSync('../src/public/output/3-extra-runs-conceded-per-team-in-2016.json', JSON.stringify(resultExtraRuns));
        })
    })
});

module.exports = router;



