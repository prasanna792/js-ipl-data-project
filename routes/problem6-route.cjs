const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
const { playerOfTheMatch } = require('../src/server/6-highest-player-of-the-match-per-season.cjs')


router.get('/problem6', (req, res) => {
    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultPlayerOfTheMatch = playerOfTheMatch(matchesFile);
        res.send(resultPlayerOfTheMatch);
        // fs.writeFileSync('../src/public/output/6-highest-player-of-the-match-per-season.json', JSON.stringify(resultPlayerOfTheMatch));
    })
});

module.exports = router;








