const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
const { numberOfTimesTeamWonTossAndMatch } = require('../src/server/5-number-of-times-team-won-toss-won-match.cjs')


router.get('/problem5', (req, res) => {
    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultWonTossMatch = numberOfTimesTeamWonTossAndMatch(matchesFile)
        res.send(resultWonTossMatch);
        // fs.writeFileSync('../src/public/output/5-number-of-times-team-won-toss-and-match.json', JSON.stringify(resultWonTossMatch));
    })

});

module.exports = router;





