
const express = require('express');
const csvtojson = require('csvtojson');
const path = require('path');
const router = express.Router();
const { matchesPerYear } = require('../src/server/1-matches-per-year.cjs')

let matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');

router.get('/problem1', (req, res) => {
    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        let resultOfMatchesPerYear = matchesPerYear(matchesFile);
        // fs.writeFileSync('../src/public/output/1-matches-per-year.json', JSON.stringify(resultOfMatchesPerYear));
        res.json(resultOfMatchesPerYear);
    });
});

module.exports = router;
