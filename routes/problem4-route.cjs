const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
let deliveriesFilePath = path.resolve(__dirname, '../src/data/deliveries.csv');
const { topTenEconomicalBowlersIn2015 } = require('../src/server/4-top-ten-economical-bowlers-in-2015.cjs')


router.get('/problem4', (req, res) => {
    csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
        csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
            let resultEconomicBowlers = topTenEconomicalBowlersIn2015(matchesFile, deliveriesFile)
            res.send(resultEconomicBowlers);
            // fs.writeFileSync('../src/public/output/4-top-ten-economic-bowlers-in-2015.json', JSON.stringify(resultEconomicBowlers));
        })
    })
});

module.exports = router;





