const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

let deliveriesFilePath = path.resolve(__dirname, '../src/data/deliveries.csv');
const { dismissedByAnotherPlayer } = require('../src/server/8-highest-number-of-times-player-dismissed-by-another-player.cjs')


router.get('/problem8', (req, res) => {
    csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
        let resultHighDismissal = dismissedByAnotherPlayer(deliveriesFile);
        res.send(resultHighDismissal);
        // fs.writeFileSync('../src/public/output/8-highest-number-of-times-player-dismissed-by-another-player.json', JSON.stringify(resultHighDismissal));
    })
});

module.exports = router;




