const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
const { matchesWonPerTeamPerYear } = require('../src/server/2-matches-won-per-team-per-year.cjs')


router.get('/problem2', (req, res) => {
    csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
        const resultMatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matchesFile);
        res.send(resultMatchesWonPerTeamPerYear);
        // fs.writeFileSync('../src/public/output/2-matches-won-per-team-per-year.json', JSON.stringify(resultMatchesWonPerTeamPerYear));
    })
    
});

module.exports = router;