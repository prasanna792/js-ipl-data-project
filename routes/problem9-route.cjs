const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

let deliveriesFilePath = path.resolve(__dirname, '../src/data/deliveries.csv');
const { bestEconomyInSuperOvers } = require('../src/server/9-bowler-with-best-economy-in-super-overs.cjs')


router.get('/problem9', (req, res) => {
    csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
        let resultBestEconomy = bestEconomyInSuperOvers(deliveriesFile);
        res.send(resultBestEconomy);
        // fs.writeFileSync('../src/public/output/9-best-economy-in-superovers.json', JSON.stringify(resultBestEconomy));
    })
});

module.exports = router;



