const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');
const express = require('express');
const router = express.Router();

const matchesFilePath = path.resolve(__dirname, '../src/data/matches.csv');
let deliveriesFilePath = path.resolve(__dirname, '../src/data/deliveries.csv');
const { strikeRate } = require('../src/server/7-strike-rate-of-batsman-for-each-season.cjs')


router.get('/problem7', (req, res) => {
    csvtojson().fromFile(deliveriesFilePath).then((deliveriesFile) => {
        csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
            let resultStrikeRate = strikeRate(matchesFile, deliveriesFile)
            res.send(resultStrikeRate);
            // fs.writeFileSync('../src/public/output/7-strike-rate-of-batsman-for-each-season.json', JSON.stringify(resultStrikeRate));
        })
    })

});

module.exports = router;









